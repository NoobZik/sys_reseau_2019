/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex01.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/31 15:52:20 by NoobZik           #+#    #+#             */
/*   Updated: 2019/01/31 16:28:49 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>

#define N 4

pthread_t pthread_id[N][N];

void generation_aleatoire(int matrice[N][N]);
void imprime(int matrice[N][N]);
void *somme(void *arg);

void generation_aleatoire(int matrice[N][N]) {
  int i, j;
  for (i = 0; i < N; i++) {
    for (j = 0; j < N; j++) {
      matrice[i][j] = rand() % 100;
    }
  }
}

void imprime(int matrice[N][N]) {
  int i, j;

  for (i = 0; i < N; i++) {
    for (j = 0; j < N; j++) {
      printf("%d", matrice[i][j]);
    }
    printf("\n");
  }
}

void *somme(void *arg) {
  *((int **)arg)[0] =  *((int **)arg)[1] + *((int **)arg)[2];
  return arg;
}

int main(int argc, char const *argv[]) {
  (void) argc;
  (void) argv;
  srand((unsigned int) time(NULL));
  int m1[N][N];
  int m2[N][N];
  int m3[N][N];
  int i, j;
  generation_aleatoire(m1);
  generation_aleatoire(m2);
  puts("m1 :");
  imprime(m1);
  puts("m2");
  imprime(m2);

  int **arg = malloc(sizeof(int)*3);

  for (i = 0; i < N; i++) {
    for (j = 0; j < N; j++) {
        arg[0] = &(m1[i][j]);
        arg[1] = &(m2[i][j]);
        arg[2] = &(m3[i][j]);
        if(pthread_create(&pthread_id[i][j], NULL, somme, (void *) arg) == -1)
          fprintf(stderr, " Erreur de creation du pthread numero (%d %d)", i,j);
    }
  }
  sleep(1);
  free(arg);
  imprime(m3);
  return 0;
}
