/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Serveur_TCP.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/16 17:16:09 by NoobZik           #+#    #+#             */
/*   Updated: 2019/05/19 16:05:02 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
 * SHEIKH Rakib 11502605
 * YESLI Sofiane 11507199
 */

/* serveur_TCP.c (serveur TCP) */
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <assert.h>
#include <unistd.h>

#define NBECHANGE 3
#define _GNU_SOURCE
const size_t MAX_LINE = 2048;

char* id = 0;
short port = 0;
int sock = 0; /* socket de communication */
int nb_reponse = 0;

typedef struct Annonce {
  char *adresse;
  char *texte;
  struct Annonce *suivant;
} Annonce_t;

Annonce_t *liste_annonces;
int ajout(char *adresse, char *texte);
int suppression(Annonce_t *annonce);
char *lister(void);
size_t longueur(void);
void traitement_commande(char *, char *);

size_t size(Annonce_t *annonce);
 /* Pour le strtok, coupage de chaines de caractère pour buf_read */
char delimit[]=" \t\r\n\v\f";

int main(int argc, char** argv) {
  struct sockaddr_in serveur; /* SAP du serveur */
  //char *buf_write = malloc(sizeof(MAX_LINE));
  if (argc != 3) {
      fprintf(stderr,"usage: %s id port\n",argv[0]);
      exit(1);
  }
  id = argv[1];
  port = (short int) atoi(argv[2]);
  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
      fprintf(stderr,"%s: socket %s\n", argv[0],strerror(errno));
      exit(1);
  }
  serveur.sin_family = AF_INET;
  serveur.sin_port = htons( (uint16_t) port);
  serveur.sin_addr.s_addr = inet_addr(argv[1]);
	if (bind(sock,(struct sockaddr *) &serveur, sizeof(serveur)) < 0) {
        fprintf(stderr,"%s: bind %s\n", argv[0],strerror(errno));
        exit(1);
  }
  if (listen(sock, 5) != 0) {
    fprintf(stderr,"%s: listen %s\n", argv[0],strerror(errno));
    exit(1);
  }
  while (1) {
    struct sockaddr_in client; /* SAP du client */
    int len = sizeof(client);
    int sock_pipe; /* socket de dialogue */
    int ret;

    sock_pipe = accept(sock,(struct sockaddr *) &client, (socklen_t *) &len);
    while(1) {
      char buf_read[1<<8], buf_write[1<<8];

      ret = (int) read(sock_pipe, buf_read, sizeof(buf_read));
      if (ret <= 0) {
        printf("%s: read=%d: %s\n", argv[0], ret, strerror(errno));
        break;
      }
      printf("srv %s recu de (%s,%4d):%s\n", id, inet_ntoa(client.sin_addr), ntohs(client.sin_port), buf_read);
      memset(buf_write, 0, sizeof(buf_write));
      traitement_commande(buf_read, buf_write);
      //  assert(*buf_write);
      //  sprintf(buf_write,"#%2s=%03d#", id,nb_reponse++);
      ret = (int) write(sock_pipe, buf_write, sizeof(buf_write));
      if (ret <= 0) {
        printf("%s: write=%d: %s\n", argv[0], ret, strerror(errno));
        break;
      }
      sleep(2);
    }
    close(sock_pipe);
  }
  return 0;
}


/**
 * Analyseur syntaxique d'une ligne de commande recu.
 * Le premier argument est la commande, mise dans command.
 * Le deuxieme argument est l'adresse, mise dans adresse.
 * Le troisieme argument est un texte, mise dans texe.
  *
 * Les commandes gèrent les majuscules/minuscules.
 * La fonction recoit en paramètre une chaine d'entré (client) reçu et une
 * chaine de  sortie (serveur) à envoyer
 *
 * @param char *buffer Chaine d'entrée contenant les commandes, arguments envoyé
 *        par le client.
 * @param buf_write Chaine de sortie à envoyer commen réponse au client, depuis
 *        depuis le serveur.
 *
      */
void traitement_commande(char *buffer, char *buf_write) {
  char *token;
  char *rest = buffer;
  char command[1<<8];
  char texte[1<<8];
        char adresse[1<<8];
       int res;

  token = strtok_r(rest, delimit, &rest);
  strcpy(command, token);
  /* LISTER */
  if ((strcmp(command, "LISTER") == 0) || (strcmp(command, "lister") == 0)) {
  //  printf("je suis passé ici\n");
    token = lister();
    sprintf(buf_write,"%s",token);
  //free(token);
    return;
  }
  /* AUTRES CAS */
  else {
    if (strcmp(command, "SIZE") == 0 || strcmp(command, "size") == 0) {
      sprintf(buf_write, "Taille de l'annonce actuelle : %lu\n", size(liste_annonces));
      return;
    }

    /* Tous les commands sans arguments sont traité, on continue de parser la suite */

    token = strtok_r(rest, delimit, &rest);
    if (token == NULL) {
      sprintf(buf_write, "Il manque arg adresse ou texte");
      return;
    }
    memset(adresse, 0, 1<<8);
    strcpy(adresse, token);

    token = strtok_r(rest, "\0", &rest);
    if (texte == NULL) {
      sprintf(buf_write, "Il manque arg adresse ou texte");
      return;
    }
    memset(texte, 0, 1<<8);
    strcpy(texte,token);

    /* Ajout */

    if (strcmp(command, "AJOUTER") == 0 || strcmp(command, "ajouter") == 0) {
      if ((res = ajout(adresse, texte)) == -1) {
          sprintf(buf_write, "Erreur -1 à l'ajout");
          return;
      }

      else if (res == 0){
        sprintf(buf_write, "Ajouté : (%s:%s)", adresse, texte);
        printf("%s\n", buf_write);
        return;
      }

      else {
        sprintf(buf_write, "Erreur d'ajout, unexpected");
        return;
      }
    }

    /* Suppresion */

    if (strcmp(command, "suppression") == 0 || strcmp(command, "SUPPRESSION") == 0) {
      Annonce_t *annonce = malloc(sizeof(Annonce_t));
      annonce->texte = malloc(sizeof(MAX_LINE));
      annonce->adresse = malloc(sizeof(MAX_LINE));
      strcpy(annonce->texte, texte);
      strcpy(annonce->adresse, adresse);

      /* Gerer tout les cas d'erreur de la fonctions */

     if ((res = suppression(annonce)) == -1) {
        sprintf(buf_write, "Erreur -1 : Annonce non existant en arg");
        return;
      }
      else if (res == -2){
        sprintf(buf_write, "Erreur -2 : Liste d'annonce vide");
        return;
      }

      else {
        if (res == 0) {
          sprintf(buf_write, "Suppresion effectué OK");
          return;
        }

        else {
          sprintf(buf_write, "Erreur inconnu");
          return;
        }
      }
    }
  }
}

/**
 * [ajout description]
 * @param  adresse [description]
 * @param  texte   [description]
 * @return         [description]
 */
int ajout(char *adresse, char *texte) {
  Annonce_t *annonce;
  if ((annonce = (Annonce_t *) malloc (sizeof(Annonce_t))) == NULL) return -1;
  annonce->adresse = malloc(1<<10);
  annonce->texte = malloc(1<<10);
  strcpy(annonce->adresse, adresse);
  strcpy(annonce->texte, texte);
  annonce->suivant = liste_annonces;
  liste_annonces = annonce;
  printf("Ajouté dans la liste : %s\t%s\n", liste_annonces->adresse, liste_annonces->texte);
  return 0;
}

/**
 * [suppression description]
 * @param  annonce [description]
 * @return         [description]
 */
int suppression(Annonce_t *annonce) {
  if (annonce == NULL) return -1;
  if (liste_annonces == NULL) return -2;
  Annonce_t *tmp = liste_annonces;
  Annonce_t *tmp_pred;
  if (strcmp(tmp->adresse,annonce->adresse) == 0 && strcmp(tmp->texte,annonce->texte) == 0) {
    liste_annonces = liste_annonces->suivant;
    return 0;
  }
  tmp_pred = tmp;
  tmp = tmp->suivant;
  while (tmp != NULL) {
    if (strcmp(tmp->adresse,annonce->adresse) == 0 && strcmp(tmp->texte,annonce->texte) == 0) {
      tmp_pred->suivant = tmp->suivant;
      free(tmp->adresse);
      free(tmp->texte);
      free(tmp);
      return 0;
    }
    tmp_pred = tmp;
    tmp = tmp->suivant;
  }
  return -2;
}

/**
 * Liste l'annuaire d'annonce sous forme de liste
 * @return  [description]
 */
char *lister(void) {
  char *s;
  size_t longu;
  Annonce_t *tmp = liste_annonces;
  if ((s = (char *) malloc (longu = longueur() * sizeof(char) + 1)) == NULL) return NULL;
  if (tmp == NULL) return (char *) "vide";
  memset(s, 0, longu);
  while (tmp != NULL) {
  //    printf("Ajout dans le buffer lister : %s\t%s\n", tmp->adresse, tmp->texte);
    assert(tmp->adresse && s);
    strcat(s,tmp->adresse);
    strcat(s,"\t");
    strcat(s,tmp->texte);
    strcat(s,"\n");
  // fix condiational jump
    if (tmp->suivant == NULL)
      tmp = NULL;
    else
      tmp = tmp->suivant;
  }
  return s;
}
/**
 * Calcul la longueur pour generer une chaine de caractère de sortie.
 * @return  [description]
 */
size_t longueur(void) {
  size_t l = 0;
  Annonce_t *tmp = liste_annonces;
  while (tmp != NULL) {
    l += strlen(tmp->adresse) + 1 + strlen(tmp->texte) + 1;
    tmp = tmp->suivant;
  }
  return l;
}

/**
 * size()
 *
 * Retourne la taille de la liste d'annonce (à titre de débug)
 * @param annonce Annonce_t
 * return size_t taille de la liste.
 */
size_t size(Annonce_t *annonce) {
  Annonce_t *tmp = annonce;
  if (tmp == NULL) return 0;
  else {
    puts("Entré dans size else");
    size_t i = 1;
    while ((tmp = tmp->suivant) != NULL && i++);
    return i;
  }
}
