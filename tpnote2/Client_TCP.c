/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Client_TCP.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/16 17:16:09 by NoobZik           #+#    #+#             */
/*   Updated: 2019/05/19 20:22:02 by noobzik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
 * SHEIKH Rakib 11502605
 * YESLI Sofiane 11507199
 */

 /* client_TCP.c (client TCP) */

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>

#define NBECHANGE 3
#define _GNU_SOURCE

char* id = 0;
short sport = 0;
int sock = 0; /* socket de communication */

int main(int argc, char** argv) {
  struct sockaddr_in moi;      /* SAP du client */
  struct sockaddr_in serveur;  /* SAP du serveur */
  //  int nb_question = 0;
  int ret,len;

  if (argc != 4) {
    fprintf(stderr,"usage: %s id serveur port\n",argv[0]);
    exit(1);
  }
  id = argv[1];
  sport = (short int) atoi(argv[3]);
  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
    fprintf(stderr,"%s: socket %s\n", argv[0],strerror(errno));
    exit(1);
  }
  if (setsockopt(sock,SOL_SOCKET,SO_REUSEADDR,&ret,sizeof(int)) == -1) {
    perror("setsockopt");
    exit(1);
  }
  serveur.sin_family = AF_INET;
  serveur.sin_port = htons((uint16_t)sport);
  inet_aton(argv[2], &serveur.sin_addr);
  if (connect(sock, (struct sockaddr *) &serveur, sizeof(serveur)) < 0) {
    fprintf(stderr,"%s: connect %s\n", argv[0],strerror(errno));
    perror("bind");
    exit(1);
  }
  len = sizeof(moi);
  getsockname(sock,(struct sockaddr *) &moi,(socklen_t *) &len);
  while (1) {
    char buf_read[1<<8], buf_write[1<<8];
    puts("Saissie de commande :");
    fgets(buf_write, 1<<8, stdin);
    buf_write[strcspn(buf_write, "\n")] = 0;
//      sprintf(buf_write,"#%2s=%03d",id,nb_question);
    printf("client %2s: (%s,%4d) envoie a ",id, inet_ntoa(moi.sin_addr), ntohs(moi.sin_port));
    printf(" (%s,%4d) : %s\n", inet_ntoa(serveur.sin_addr), ntohs(serveur.sin_port), buf_write);
    ret = (int) write(sock, buf_write, sizeof(buf_write));
    if (ret <= (int) strlen(buf_write)) {
      printf("%s: erreur dans write (num=%d, mess=%s)\n",argv[0],ret,strerror(errno));
      continue;
    }
    printf("client %2s: (%s,%4d) recoit de ", id, inet_ntoa(moi.sin_addr), ntohs(moi.sin_port));
    ret = (int) read(sock, buf_read, sizeof(buf_read));
    if (ret <= 0) {
        printf("%s:erreur dans read (num=%d,mess=%s)\n", argv[0],ret,strerror(errno));
        continue;
    }
    printf("(%s,%4d):%s\n",inet_ntoa(serveur.sin_addr), ntohs(serveur.sin_port),buf_read);
  }
  close(sock);
  return 0;
}
