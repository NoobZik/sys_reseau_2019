/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex01.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/27 11:03:51 by NoobZik           #+#    #+#             */
/*   Updated: 2019/02/27 11:50:26 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
 *  Programme du quel le processus père envoie SIGINT (CRTL+C) a son processus
 *  fils avec la fonction kill après avoir testé son existence.
 */

#define _POSIX_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>



int main(void) {

  pid_t pid;

  /* Le processus fils */
  if ((pid = fork()) == 0) {
    while (1);
  }

  /* Le processus pere*/
  else {
    if (kill(pid, SIGINT) != 0) {
      perror("erreur kill");
      exit(-1);
    }
    else {
      puts("Kill success");
    }
  }
  return 0;
}
