/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Correction_ex02.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/28 16:49:31 by NoobZik           #+#    #+#             */
/*   Updated: 2019/02/28 17:01:29 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#define _POSIX_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>

#define NMAX 5

void fonction_signalpere(int sig);
void signal_fils (int sig);

int nb_sgusr1 = 0;
pid_t pid_fils;

/**
 * [fonction_signalpere description]
 * @param sig [description]
 */
void fonction_signalpere(int sig) {
  if (sig == SIGUSR1) {
    fprintf(stderr, "Reception du signal SIGUSR1\n");
    fprintf(stderr, "nb SIGUSR1:%d\n", ++nb_sgusr1);
    if (nb_sgusr1 == NMAX) {
      fprintf(stderr, "Envoi du signal SIGUSR2\n");
      kill(pid_fils,SIGUSR2);
      exit(0);
    }
  }
}

/**
 * [signal_fils  description]
 * @param sig [description]
 */
void signal_fils (int sig) {
  if (sig == SIGUSR2) {
    fprintf(stderr, "Reception du signal SIGUSR2\n");
    fflush(stderr);
    exit(0);
  }
}


int main(int argc, char const *argv[]) {
  (void) argc;
  (void) argv;
  switch (pid_fils = fork()) {
    case (pid_t) -1: //cast
      perror("creation de processus");
      exit(2);
    case (pid_t) 0: //cast
      fprintf(stderr, "%Lancement du fils\n");
      if (signal(SIGUSR2, signal_fils) == SIG_ERR) {

      }

  }
  return 0;
}
