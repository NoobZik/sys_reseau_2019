/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex02.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/27 11:49:30 by NoobZik           #+#    #+#             */
/*   Updated: 2019/02/28 16:47:58 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#define _POSIX_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>
/*
void signal_handler (int);
*/
void signal_parent(int sig);
void signal_child(int sig);
int counter = 0;
pid_t pid;
/*
void signal_handler(int signal) {

  if (signal == SIGINT) {
    if (counter < 3) {
      counter++;
      printf("counter %d\n", counter);
    }
    else
       exit(0);
  }
}*/

void signal_parent(int sig) {
  if (sig == SIGUSR1) {
    if (counter < 3) {
      counter++;
      printf("counter %d\n", counter);
    }
    else {
      puts("Sending SIGUSR2 to child process");
      kill(pid, SIGUSR2);
      exit(0);
    }
  }
}

void signal_child(int sig) {
  if (sig == SIGUSR2) {
    puts("Exiting child process");
    exit(0);
  }
}
int main (void) {
  //signal(SIGUSR1, signal_handler);
  pid = fork();
  /* Processus fils */

  if (pid == 0) {
    signal(0, signal_parent);
    while(1) {
      kill(pid, SIGUSR1);
      puts("Signal sent from child");
      sleep(5);
    }
  }

  /* Processus père */
  else {
    while (1) {
    //  signal(SIGINT, signal_handler);
      signal(0, signal_child);
      //kill(SIGUSR1, pid);
      puts("Signal sent from parent");
      sleep(2);
    }
  }
  return 0;
}
