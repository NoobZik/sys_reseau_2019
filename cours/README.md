# Système et réseaux #

## Jeudi 28 févriver ##


### Tubes ###

Threads : Mémoire globale partagé. Envoyer une valeur vers un autre thread suffit juste a mettre à jour la variable

Processeur : Il y a 3 trucs

-   On peut définir une mémoire partagée (Avec les variables globales par exemple).
Et avec des fonctions systèmes spécifiques.
-   Tube non nommées
-   Tubes nommées.

A chaque processus, est associé une table de descripteurs de fichiers.
Chaque entrée de la table contient :
-   L'adresse (position courante) dans le fichiers
-   L'indice dans la table de descripteurs de fichiers.

La talbe des descriptions de fichier est unique et directement géré par le OS. Chaque entrée contient
-   L'adresse physique du fichiers
-   Les droits associés
-   Nombres de processus accédant a ce fichier
-   Adresse des code de lecture/ecriture
-   Nom de fichier


### Tube non nommé ###

```c
int pipe(int tab[2])
```

-   Initialise un tampon en mémoire centrale (entré dans la table des descripteurs de fichiers) non associé à un fichier sur périphérique
-   Ouverture unidirectionnel

tab\[0\] C'est l'indice de la table des descripteurs de fichiers en lecture seul.
tab\[1\] C'est l'indice de la table des descripteurs de fichiers en ecriture seul

C'est comme si on faisait un double open sur un fichier fictif (en lecture seul et en ecriture seul respectivement).

Le tampon en mémoire centrale c'est une zone de mémoire centrale (valable pour toute entré de la table de descripteurs de fichiers). Sa taille est en général 8Ko.

La fonction :
```c
flush();
```
demande à executer "immédiatement" les opérations sur ce tampon
fork() duplique les info associées à 1 processus donc aussi la table de descripteurs de fichiers.

Par défault :
-   entrée 0 = lecture claver
-   entrée 1 = sortie ecran
-   entrée 2 = sortie en erreur (par défaut ecran)
-   entrée >2 = Null

Il faut deux tube pipe pour les faire communiquer (et non 1).
-   Un pipe du processus A ---> processus B
-   Un pipe du processus B ---> processus A

##### Tube nom nommées #####

L'avantage est que c'est facile à utilisé et que c'est rapide (car c'est la mémoire centrale) .0
L'inconvéniant est au nombre de 2 :
-   La taille du tampon est limité
-   Execution synchrone des processus.

##### Tube nommée #####
Peudo-fichier sur disque donc nommée et fonctionne comme un tube. (FIFO)

---------------
---->     ---->
---------------

Execution asynchrone des processus.

La fonction
```c
/**
 * [Retour description]
 * @param const char*  Nom du noeud
 * @param mode_t Type de mode de création du noeud S_IFIFO
 * @param dev_t Adresse du périphérique
 *
 * @type {[type]}
 */
int mknod (const char * path, mode_t, dev_t, dev);
```
Retour :
-   0 Si c'est OK
-   <0 En cas d'erreur

Le premier argument est le nom du fichier. (ici du pseudo-fichier)
Le deuxieme argument est

S_FIFO = mode fifo (Avec tube)
S_IFREG : regular file

L'adresse du périphérique sous UNIX sont souvent crée dans /dev
Lorsque le système démarre, a chacun des périphérique est associé un nom codé dans /dev
Si on met null, la fonction va déduire quel est le périphérique est à utiliser.
