
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<pthread.h>
#include<semaphore.h>
#include <string.h>
#include <errno.h>

typedef struct noeud {
     int valeur;
     struct noeud *suivant;
   } noeud_t;

typedef struct liste {
     noeud_t *liste;
   } liste_t;


   typedef struct
   {
   	sem_t	sem_fifo;		// contrôle un accès par un seul thread aux sections critiques
   	sem_t	sem_ecrivain;	// contrôle qu'un écrivain peut écrire
   	sem_t	sem_nbLecteurs;	// contrôle l'accès à la variable nbLecteurs
   	int		nbLecteurs;		// nombre de lecteurs en cours
   } ControleLecteurEcrivain;

void ajouter(int);
void retirer(int);
int tester(int);
void imprimer(void);
void initialisecontrole(ControleLecteurEcrivain*);
void finEcriture(ControleLecteurEcrivain*);
void debutEcriture(ControleLecteurEcrivain*);
void finLecture(ControleLecteurEcrivain*);
void debutLecture(ControleLecteurEcrivain*);
void *tacheAjouter(void*);
void *tacheLecteur(void*);
void *tacheRetirer(void*);
liste_t* maListe;


ControleLecteurEcrivain controle;


void initialisecontrole(ControleLecteurEcrivain* res) {
  sem_init(&res->sem_fifo, 0, 1);
  sem_init(&res->sem_ecrivain, 0, 1);
  sem_init(&res->sem_nbLecteurs, 0, 1);
  res->nbLecteurs = 0;
}

void debutLecture(ControleLecteurEcrivain* res) {
  if (sem_wait(&res->sem_fifo) == -1) {
    perror("sem_wait 1 debutLecture :");
    exit(-1);
  }
  if (sem_wait(&res->sem_nbLecteurs) == -1) {
    perror("sem_wait 2 debutLecture");
    exit(-1);
  }

  res->nbLecteurs += 1;

  if (res->nbLecteurs == 1)
    sem_wait(&res->sem_ecrivain);

  sem_post(&res->sem_nbLecteurs);
  sem_post(&res->sem_fifo);
}

void finLecture(ControleLecteurEcrivain* res) {
//  if (
    sem_wait(&res->sem_nbLecteurs); //!= -1
  //) {
  //  puts("Erreur sem_wait fin de lecture");
  //  puts(strerror(errno));
  //  exit(-1);
  //}
  /**
   * Quand la lecture du sem est à 0, on doit faire une libération du sémaphore
   * ecrivain
   */
  if ((res->nbLecteurs -= 1) == 0) {
    sem_post(&res->sem_ecrivain);
  }
  sem_post(&res->sem_nbLecteurs);
}

void debutEcriture(ControleLecteurEcrivain* res) {
  if (sem_wait(&res->sem_fifo) == -1) {
    perror("sem_wait debutecriture fifo:");
    exit(-1);
  }
  if (sem_wait(&res->sem_ecrivain) == -1) {
    perror("sem_wait debutecriture ecrivain:");
    exit(-1);
  }
}

void finEcriture(ControleLecteurEcrivain* res) {
  sem_post(&res->sem_ecrivain);
  sem_post(&res->sem_fifo);
}

void *tacheAjouter(void* arg) {
	int n;
	n = atoi((char *)arg);

	debutEcriture(&controle);
	printf("Tache : ajout de %d\n",n);
	ajouter(n);
	finEcriture(&controle);

	pthread_exit (NULL);
}

void *tacheRetirer(void* arg) {
	int n;
	n = atoi((char *)arg);

	debutEcriture(&controle);
	printf("Tache : suppression de %d\n",n);
	retirer(n);
	finEcriture(&controle);

	pthread_exit (NULL);
}

void *tacheLecteur(void* arg) {
	int n;
	n = atoi((char *)arg);

	debutLecture(&controle);
	printf("Tache %d: lecture\n",n);
	imprimer();
	finLecture(&controle);

	pthread_exit (NULL);
}


int main (int argc, char* argv[]) {
	maListe = (liste_t *) malloc (sizeof(liste_t));
	maListe->liste = NULL;
  (void) argc;
  (void) argv;
	pthread_t t;
	initialisecontrole(&controle);

	if (pthread_create(&t, NULL, tacheAjouter, (void*)"3")!=0)
			exit(-1);
	if (pthread_create(&t, NULL, tacheAjouter, (void*)"1")!=0)
			exit(-1);
	if (pthread_create(&t, NULL, tacheLecteur, (void*)"0")!=0)
			exit(-1);
	if (pthread_create(&t, NULL, tacheRetirer, (void*)"1")!=0)
			exit(-1);
	if (pthread_create(&t, NULL, tacheLecteur, (void*)"1")!=0)
			exit(-1);

	pthread_exit(NULL);
}

int tester(int v) {
	noeud_t* courant;
	int ret;

	courant = maListe->liste;
	while (courant != NULL && courant->valeur != v) {
		courant = courant->suivant;
	}
	if (courant == NULL) ret = 0; else ret = 1;
	return ret;
}

void imprimer(void) {
	noeud_t* courant;

	courant = maListe->liste;
	printf("Liste:");
	while (courant != NULL) {
		printf(" %d",courant->valeur);
		courant = courant->suivant;
	}
	printf("\n");
}

void ajouter(int v) {
	noeud_t* courant;
	if (tester(v) == 0) { // v n'est pas dans la liste
		noeud_t * nouveau = (noeud_t *) malloc (sizeof(noeud_t));
		nouveau->valeur = v;


		if (maListe->liste == NULL) { // la liste est vide
			nouveau->suivant = NULL;
			maListe->liste = nouveau;
		} else {
			courant = maListe->liste;
			if (courant->valeur > v) { // v est le nouveau premier element
				nouveau->suivant = maListe->liste;
				maListe->liste = nouveau;
			} else {
				while (courant->suivant != NULL && courant->suivant->valeur < v) {
					courant = courant->suivant;
				}
				nouveau->suivant = courant->suivant;
				courant->suivant = nouveau;
			}
		};
	}
}


void retirer(int v){
	noeud_t* courant;
	if (tester(v) == 1) { // v est dans la liste


		courant = maListe->liste;
		if (courant->valeur == v) { // on supprime le premier element
			maListe->liste = courant->suivant;
			free(courant);
		} else {
			while (courant->suivant != NULL && courant->suivant->valeur != v) {
				courant = courant->suivant;
			}
			if (courant->suivant !=NULL && courant->suivant->valeur == v) {
				noeud_t * aEnlever = courant->suivant;
				courant->suivant = aEnlever->suivant;
				if (aEnlever != NULL) free(aEnlever);
			}
		};
	}
}
