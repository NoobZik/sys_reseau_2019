
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<pthread.h>
#include<semaphore.h>

#define TRUE 1

#define NB_PRODUCTEUR 3			// nombre de producteurs
#define NB_CONSOMMATEUR 3		// nombre de consommateurs

#define NB_PLACES 10			// nombre d'entrées dans le tampon

int tampon[NB_PLACES];

typedef struct
{
	sem_t	sem_acces;			// contrôle d'accès en section critique
	sem_t	sem_vide;			// nombre d'entrées vides
	sem_t	sem_plein;			// nombre d'entrées occupées
	int		num_occupe;			// dernière entrée libre dans le tampon
	int		num_libre;			// première entrée libre dans le tampon
	void* 	tampon[NB_PLACES];	// tampon des données à traiter	
} ControleProdConso;

ControleProdConso controle;

void mettre(void *arg);
void retirer(void **arg);
void produire(void **arg);
void utiliser(void *arg);

void* producteur (void* arg)
{
	void* donnee;
	int i;
	for (i=0;i<10;i++) {
		produire (&donnee) ; 
		???????
		mettre (donnee) ; 
		???????
	}
	pthread_exit(NULL);
} 

void* consommateur (void* arg) 
{
	void* donnee;
	int i;
	for (i=0;i<10;i++) {
		???????
		retirer (&donnee) ; 
		???????
		utiliser (donnee) ;
	}
	pthread_exit(NULL);
}


void mettre(void *arg)
{
	controle.tampon[controle.num_libre] = arg;
	controle.num_libre = (controle.num_libre + 1) % NB_PLACES;
	if (controle.num_occupe == controle.num_libre)
		printf("TAMPON TOTALEMENT PLEIN !\n");
}

void retirer(void **arg)
{
	controle.num_occupe = (controle.num_occupe + 1) % NB_PLACES;
	*arg = controle.tampon[controle.num_occupe];
	if ((controle.num_occupe +1) % NB_PLACES == controle.num_libre)
		printf("TAMPON TOTALEMENT LIBRE !\n");
}

void produire(void **arg)
{
	int* donnee = (int *) malloc (sizeof (int)); 
	*donnee = rand() % 50;
	*arg = (void *)donnee;
	printf("Production de %d\n",*donnee);
}

void utiliser(void *arg)
{
	printf("Utilisation de %d\n",*(int *)arg);
	free(arg);
}

void initialisecontrole (ControleProdConso* controle)
{
		???????
}

int main (int argc, char* argv[]) {
 
	pthread_t t;
	int i;

	initialisecontrole(&controle);

	for (i=0;i<NB_PRODUCTEUR;i++)
	{
		if (pthread_create(&t, NULL, producteur, NULL)!=0)
				exit(-1);
	}
	for (i=0;i<NB_CONSOMMATEUR;i++)
	{
		if (pthread_create(&t, NULL, consommateur, NULL)!=0)
				exit(-1);
	}
	pthread_exit(NULL);
}


