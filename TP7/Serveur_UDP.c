/* serveur_UDP.c (serveur UDP) */
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>


 char* id = 0;
 short port = 0;
 int sock = 0; /* socket de communication */
 int nb_reponse = 0;

 int main(int argc, char** argv) {
     int ret;
     struct sockaddr_in serveur; /* SAP du serveur */

     if (argc!=3) {
         fprintf(stderr,"usage: %s id port\n", argv[0]);
         exit(1);
         }
     id = argv[1];
     port = (short int) atoi(argv[2]);
     if ((sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
          fprintf(stderr,"%s: socket %s\n", argv[0],strerror(errno));
          exit(1);
          }
     serveur.sin_family = AF_INET;
     serveur.sin_port = (in_port_t) port;
     serveur.sin_addr.s_addr = inet_addr(argv[1]);
     if (bind(sock, (const struct sockaddr *) &serveur, sizeof(serveur)) < 0) {
         fprintf(stderr,"%s: bind %s\n", argv[0],strerror(errno));
         exit(1);
         }

     while (1) {
        struct sockaddr_in client; /* SAP du client */
        int client_len = sizeof(client);
        char buf_read[1<<8], buf_write[1<<8];

        ret = (int) recvfrom(sock, buf_read, 256, 0, (struct sockaddr *)&client, (socklen_t *)&client_len);

        if (ret <= 0) {
            printf("%s: recvfrom=%d: %s\n", argv[0],ret,strerror(errno));
            continue;
            }

         printf("serveur %s recu le msg %s de %s:%d\n", id,buf_read, inet_ntoa(client.sin_addr),ntohs(client.sin_port) );
         sprintf(buf_write, "serveur#%2s reponse%03d#", id,nb_reponse++);
         ret = (int) sendto(sock, buf_write, sizeof(buf_write), 0, (struct sockaddr *)&client, (socklen_t) client_len);
         if (ret <= 0) {
             printf("%s: sendto=%d: %s\n", argv[0],ret,strerror(errno));
             continue;
             }
         sleep(2);
         }
     return 0;
     }
