/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exo03.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/24 17:18:48 by NoobZik           #+#    #+#             */
/*   Updated: 2019/01/24 17:25:41 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define A sleep(3)

int main(int argc, char *argv[]) {
  int i;
  int n = atoi(argv[1]);
  int p = atoi(argv[2]);

  /**
   * Disregard const fix for argv[]
   */
  
  if (p == 0) {
    A;
    puts("Sortie1");
    exit(0);
  }
  for (i = 0; i < n; i++) {
    if (fork() == 0) {
      printf("pid :%d, ppid: %d et %d\n", getpid(), getppid(), p);
      sprintf(argv[2],"%d", p-1);
      main(argc,argv);
      puts("Sortie2");
      exit(0);
    }
  }

  while (wait(0) != -1);
  puts("Sortie3");
  exit(0);

  return 0;
}
