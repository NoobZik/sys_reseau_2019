/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exo02.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/24 16:31:29 by NoobZik           #+#    #+#             */
/*   Updated: 2019/01/24 17:00:13 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

/** Question 1
  *
  * En remplacant exit_pthread par exit(0), quitte le programme en supprimant
  * tout les threads associé a ce processus.
  */

void *fsomme(void *arg);
void *fproduit(void *arg);

int main(int argc, char const *argv[]) {
  (void) argc;
  (void) argv;
  int i;
  pthread_t fils[2];
  char *temp;

  temp = (char *) malloc(sizeof(int) + sizeof(char));
  sprintf(temp,"%d", 10);

  void *(*tabfonc[2]) (void*) = {
    fsomme,
    fproduit
  };

  for(i = 0; i < 2; i++) {
    if (pthread_create(&fils[i], NULL, tabfonc[i], (void *)temp))
      perror("proute");
  }

  puts("Sortie du main");
  //exit(0);
  pthread_exit(0);
}

void *fsomme(void *arg) {
  int somme = 0;
  int i;
  int n = atoi((char *)arg);
  for (i = 0; i < n; i++)
    somme += i;
  printf("Somme = %d\n", somme);
  pthread_exit(0);
}

void *fproduit(void *arg) {
  int i;
  int produit = 1;
  int n = atoi((char *) arg);
  for (i = 1; i < n; i++)
    produit *= i;
  printf("Produit = %d\n", produit);
  pthread_exit(0);
}
