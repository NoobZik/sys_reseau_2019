/* serveur_TCP.c (serveur TCP) */
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>

#define NBECHANGE 3
#define _GNU_SOURCE
#define MAX_LINE 2048

char* id = 0;
short port = 0;
int sock = 0; /* socket de communication */
int nb_reponse = 0;


void affichage_client (struct sockaddr_in);
void traitement_client (int);
void traitement_requete (int, char *);

int main(int argc, char** argv) {
  struct sockaddr_in serveur; /* SAP du serveur */

  if (argc != 3) {
    fprintf(stderr,"usage: %s id port\n",argv[0]);
    exit(1);
  }
  id = argv[1];

  port = (short int) atoi(argv[2]);
  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
    fprintf(stderr,"%s: socket %s\n", argv[0],strerror(errno));
    exit(1);
  }

  serveur.sin_family = AF_INET;
  serveur.sin_port = htons( (uint16_t) port);
  serveur.sin_addr.s_addr = inet_addr(argv[1]);


  if (bind(sock,(struct sockaddr *) &serveur, sizeof(serveur)) < 0) {
    fprintf(stderr,"%s: bind %s\n", argv[0],strerror(errno));
    exit(1);
  }


  if (listen(sock, 5) != 0) {
    fprintf(stderr,"%s: listen %s\n", argv[0],strerror(errno));
    exit(1);
  }

  while (1) {
    struct sockaddr_in client; /* SAP du client */
    int len = sizeof(client);
    int sock_pipe; /* socket de dialogue */
    int ret,nb_question;

    sock_pipe = accept(sock,(struct sockaddr *) &client, (socklen_t *) &len);

    /**
     * Quand le client est accepté sur le port 33333
     * 1 Appel de affichage_client()
     * 2 Appel de traitement_client()
     * @param nb_question [description]
     */
    // affichage_client(sock_pipe);
    for (nb_question = 0;nb_question < NBECHANGE; nb_question++) {
      char buf_read[1<<8], buf_write[1<<8];
      ret = (int) read(sock_pipe, buf_read, sizeof(buf_read));

      if (ret <= 0) {
        printf("%s: read=%d: %s\n", argv[0], ret, strerror(errno));
        break;
      }

      printf("srv %s recu de (%s,%4d):%s\n", id, inet_ntoa(client.sin_addr), ntohs(client.sin_port), buf_read);
      sprintf(buf_write,"#%2s=%03d#", id,nb_reponse++);
      ret = (int) write(sock_pipe, buf_write, sizeof(buf_write));

      if (ret <= 0) {
        printf("%s: write=%d: %s\n", argv[0], ret, strerror(errno));
        break;
      }

      sleep(2);
    }
    close(sock_pipe);
  }
  return 0;
}
/**
 * Affiche sur le terminal : L'adresse IP + port du sock
 * @param sock [description]
 */
void affichage_client(struct sockaddr_in client) {
  printf(" %s:%4d", inet_ntoa(client.sin_addr), ntohs(client.sin_port));
}

/**
 * [traitement_requete  description]
 * @param sock_l [description]
 */
void traitement_requete (int sock_l, char *requete) {
  size_t len = strlen(requete);
  int n;
  char msg[MAX_LINE];

  sprintf(msg, "Le nombre de caractère dans la requete est %lud", len);
  n = (int) send(sock_l, msg, (size_t) len, 0);

  if (n < 0) {
    perror("send error :");
    exit(-1);
  }

}
