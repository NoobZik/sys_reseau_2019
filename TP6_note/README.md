# TP Noté 1 #

Sheikh Rakib 11502605
Fraj Rayan 11607212

Compilation de tpNote.c avec :

```bash
-Wall -Wextra -pedantic -Wshadow -Wpointer-arith -Wcast-align -Wwrite-strings -Wmissing-prototypes -Wmissing-declarations -Wredundant-decls -Wnested-externs -Winline -Wno-long-long -Wold-style-definition -Wuninitialized -Wconversion -Wstrict-prototypes -std=c18
```

Compilation de tpNote_Thread.c avec :

```bash
-Wall -Wextra -pedantic -Wshadow -Wpointer-arith -Wcast-align -Wwrite-strings -Wmissing-prototypes -Wmissing-declarations -Wredundant-decls -Wnested-externs -Winline -Wno-long-long -Wold-style-definition -Wuninitialized -Wconversion -Wstrict-prototypes -lpthread -std=c18
```


Nouvelle version correctif :

-   Ajout de chdir("../") pour revenir au répertoire précédent après avoir fini avec le repertoire actuel.
