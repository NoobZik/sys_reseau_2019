//Jeffrey SY 11500780
//Louise DAUDIN 11606575

#define _DEFAULT_SOURCE

#include  <stdio.h>
#include  <stdlib.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <assert.h>

void *taille(void* n1);

int main(int argc,char const *argv[]) {
  void *res;
  pthread_t pthread_main;
  if (argc <= 1)
  {
    printf("Erreur : manque un argument");
    exit(-1);
  }
  else
  {
    pthread_create(&pthread_main, NULL, taille, (void *) argv[1]);
    pthread_join(pthread_main, &res);
    printf("Taille du repertoire : %ld \n", (off_t) res);
  }
  return EXIT_SUCCESS;
}



void *taille(void *n1)
{
  DIR* file;
  const char *nom = (const char *) n1;
  struct stat info = {0} ;
  struct dirent * objet;
  off_t val = 0;
  pthread_t sub_pthread;
  void *res;

  if((file=opendir(nom)) == NULL)
  {
    perror("Erreur ouverture\n");
    exit(-1);
  }
  printf("ouverture dossier:%s\n", (char *)nom);
  chdir(nom);
  //do {
  while ((objet = readdir(file)) != NULL)
  {
    printf("boucle\n");
    //objet=readdir(file);
    printf("%s\n",objet->d_name);

    if(objet->d_type == DT_DIR)
    {
      if (!(strncmp(objet->d_name, ".", 1) ==0|| strncmp(objet->d_name ,"..", 2)==0))
      {
        printf("objet repertoire\n");
        pthread_create(&sub_pthread,NULL,&taille, objet->d_name);
        pthread_join(sub_pthread, &res);
        val = val + (off_t) res;
        chdir("../");
      }
    }
    else if (objet->d_type == DT_REG)
      {
        stat(objet->d_name,&info);
        val = val+info.st_size;
      }

    printf("val=%ld\n",(off_t) val);
  }
  //while (objet!=NULL);

  closedir(file);
  printf("fermeture fichier");
  return (void *) val;
}
