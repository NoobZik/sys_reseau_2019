/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tpNote.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/28 17:13:15 by NoobZik           #+#    #+#             */
/*   Updated: 2019/03/01 00:21:56 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/**
 * Sheikh Rakib 11502605
 * Fraj Rayan 11607212
 */

/**
 * Nouvelle version correctif :
 * Ajout de chdir("../") pour revenir au répertoire précédent après avoir fini avec le repertoire actuel.
 */

#define _POSIX_SOURCE
#define _DEFAULT_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <assert.h>

off_t taille(const char *rep);

/**
 * This function browse all files and sub directory of the given directory
 * recursively.
 *
 * Base case : file->d_type == DT_REG
 * Recursive case : file->d_type == DT_DIR && the folder does not match with "."
 * and "..".
 *
 * @param  rep A constant pointer to a String, a directory to browse
 * @return     off_t (long unsigned int) Sum of all checked filed of the current
 *                    directory
 */
off_t taille (const char *rep){
  DIR *dir;
  struct stat file_stat;
  struct dirent *file_dir;
  off_t res = 0;

  if ((dir = opendir(rep)) == NULL) {
    puts("Failed open dir at line 53");
    puts(strerror(errno));
    exit(-1);
  }

  /**
   * Needed for stat, since the 1st arg of stat is the wrong path.
   * Update the path according to the current directory (rep).
   */
  chdir(rep);

  while ((file_dir = readdir(dir)) != NULL) {
    /**
     * Skip the . and .. directory (Recursive call)
     * @param d_type Repertory = DT_DIR
     */
    if ((file_dir->d_type == DT_DIR)) {
      if (strcmp(file_dir->d_name, ".") == 0  || (strcmp(file_dir->d_name, "..") == 0))
        continue;
      else {
        res += taille(file_dir->d_name);
        chdir("../");
      }
    }

    /**
     * Base case, when the directory DIR type is a file. (DT_REG)
     */
    if (file_dir->d_type == DT_REG) {
      if (stat(file_dir->d_name, &file_stat) == -1) {
        puts("Failed stat at line 83");
        puts(strerror(errno));
        closedir(dir);
        exit(-1);
      }
      else {
        res += file_stat.st_size;
      }
    }
  }

  closedir(dir);

  return res;
}

int main(int argc, char const *argv[]) {
  if (argc <= 1) {
    puts("No arguments\n Exiting prog..");
    exit(-1);
  }

  printf("test %ld\n", taille(argv[1]));

  return 0;
}
