#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>
#include <string.h>
#include <sys/times.h>
#include <sys/types.h>

#define ASSEZGRAND  9000

int main ()
{
  struct rlimit rlim;
  char *tab;
  int BLOCKSIZE;

  int i, j, k;
  struct tms utms;
  int start_time;
  struct rusage usage;

  /* connaitre la taille de donnee maximal d'un processus */

  getrlimit (RLIMIT_DATA, &rlim);
  fprintf (stdout, "DATA LIMIT COURANT : %lu\nDATA LIMIT MAXIMAL : %lu \n",
     rlim.rlim_cur, rlim.rlim_max);

  /* connaitre la taille d'une page (en octets) */

  BLOCKSIZE = getpagesize ();

  fprintf (stdout, "Taille d'une page est : %u\n", BLOCKSIZE);


  /* Changement de la taille de la zone de donnees */
/* A DECOMMENTER ou NON
  rlim.rlim_max=rlim.rlim_cur=BLOCKSIZE;
  setrlimit (RLIMIT_DATA, &rlim);
  getrlimit (RLIMIT_DATA, &rlim);
  fprintf (stdout, "DATA LIMIT COURANT : %lu\nDATA LIMIT MAXIMAL : %lu \n",
     rlim.rlim_cur, rlim.rlim_max);
(fin) A DECOMMENTER ou NON
*/
  /*allocation et verification */


  tab = (char *) malloc (BLOCKSIZE * ASSEZGRAND);
  if (tab == NULL)
    {
      perror ("malloc");
      exit (1);
    }

 if( memset (tab, 0, BLOCKSIZE * ASSEZGRAND)==NULL)
 {
  perror ("memset");
 }
  /*taille d'un caractere est de 1 octet */
  fprintf (stdout, "La taille d'un caractere est %lu\n", sizeof (char));

  /*lecture (assigner une valeur en memoire) en ligne puis en colonne */

  /*lancer le chrono */
  if (fork()==0) {
    fprintf (stdout, "Debut de parcours LIGNE-COLONNE: processus: %d \n",getpid());
    times (&utms);
    start_time = utms.tms_utime;
    for (i = 0; i < ASSEZGRAND; i++) {
        for (j = 0; j < BLOCKSIZE; j++) {
          k = i * BLOCKSIZE + j;
          *(tab + k) = 'a';
        }
    }
    times (&utms);
    fprintf (stdout, "Processus :%d,Le temps de parcours LIGNE-COLONNE est %ld tops d'horloge\n",
              getpid(),utms.tms_utime - start_time);
  }
  else {
      fprintf (stdout, "Debut du parcours COLONNE-LIGNE: processus %d \n",getpid());
      times (&utms);
      start_time = utms.tms_utime;
      for (j = 0; j < BLOCKSIZE; j++) {
        for (i = 0; i < ASSEZGRAND; i++)  {
          k = i * BLOCKSIZE + j;
          *(tab + k) = 'a';
        }
      }
      times (&utms);
      fprintf (stdout, "Processus :%d , Le temps de parcours COLONNE-LIGNE est %ld tops d'horloge\n",getpid(),
                utms.tms_utime - start_time);
    }
  getrusage (RUSAGE_SELF,&usage);

  fprintf (stdout,"Le nombre de demandes de pages est %ld\n",
           usage.ru_minflt);
  fprintf (stdout,"Le nombre de fautes de pages est %ld\n",
           usage.ru_majflt);

  free (tab);

  return 0;
}
