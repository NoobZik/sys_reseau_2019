# TP 4 (Informations, Commentaires) #

## Exercice 1 ##

##### getpagesize #####

getpagesize est une fonction système qui retourne un entier int. Elle requiert
l'utilisation de la librairie unistd.


Retourne la taille physique de la mémoire d'une page (RAM).

Attention : Pour getpagesize, cette fonction n'est plus une fonction standart dans la norme du posix, du coup elle va générer un warning.
Pour gerer ce warning, il faudra utiliser cette fonction a la place :

```c
sysconf( \_SC\_PAGESIZE);
```

##### memset #####

Cette fonction prends en argument un pointeur générique de la donnée que l'on veut traiter, ensuite l'écriture de l'octet en int (On pense a bien caster selon la table ASCII) et la longueur de l'écriture.

Cette fonction renvoie un pointer générique qui est exactement le même que celui donnée en paramètre !.

##### time #####



## Exercice 2 ##

Apparement, on dit que c'est un défaut de page lorsque cette page est vide.
