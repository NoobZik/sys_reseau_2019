/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex01.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/14 17:26:15 by NoobZik           #+#    #+#             */
/*   Updated: 2019/02/27 10:13:48 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/times.h>
#include <sys/resource.h>
#include <stdint.h>
#include <errno.h>

#define ASSEZGRAND  90

int main(void) {

  struct rlimit *r;

  long int BLOCKSIZE;
//  size_t arraysize;
  char *array;

  int i, j;
  long int k;

  /* Pour la question 5, calcul du temps */
  struct tms utms;
  long int start_time;
  struct rusage usage;

  /* Malloc of struc rlimit since it's an edited value by the call
  of getrlimit(int (MACRO RESSOURCE),struct rlimit *) given by its parameters */

  r = malloc(sizeof(struct rlimit));

  /* Warning, nested extern declaration of getpagesize */
  /* Warning, implicit declaration of function getpagesize */
  //int BLOCKSIZE = getpagesize();

  /* Cette fonction est la nouvelle norme pour les appels systèmes.*/
  BLOCKSIZE = sysconf(_SC_PAGESIZE);

  printf("Blocksize %ld\n", BLOCKSIZE);


  /* getrlimit return two things :
   * 0 if the function worked correctly
   * -1 if not + set the error type on errno
   */
  int p = getrlimit(RLIMIT_DATA, r);

  (p == 0) ? (void) printf("Current : %ju \nMax : %ju\n",
  (uintmax_t) r->rlim_cur, (uintmax_t) r->rlim_max) : (void) strerror(errno);

  /* Diminution de taille */
  /* Mise en commentaire, sinon le malloc échoue a partir de 33 */
/*
  r->rlim_cur = 5;
  r->rlim_max = 10;
  setrlimit(RLIMIT_DATA, r);
  p = getrlimit(RLIMIT_DATA, r);


  (p == 0) ? (void) printf("Current : %ju \nMax : %ju\n",
  (uintmax_t) r->rlim_cur, (uintmax_t) r->rlim_max) : (void) strerror(errno);
*/
  /* Size of the char * size of the initial array * blocksize */
  array = (char *) malloc((size_t) BLOCKSIZE * ASSEZGRAND);
  if(array == 0) {
    perror("Array malloc error line 78\n");
    free(r);
    exit(-1);
  }

  /* https://www.geeksforgeeks.org/memset-c-example/ */

  /* memset renvoie un pointer générique identique à celui passé en param 1 */
  array = (char *) memset(array, 0, (size_t) BLOCKSIZE * ASSEZGRAND);

  /* if = processus fils */
  if (fork() == 0) {
    printf("PID : %d\n", getpid());
    /* */
    times(&utms);
    start_time = (int) utms.tms_utime;
    /* Partie du code donnée du sujet*/
    for (i = 0; i < ASSEZGRAND; i++) {
      for (j = 0; j < BLOCKSIZE; j++) {
        k = (int) i * BLOCKSIZE + j;
        *(array + k) = 'a';
      }
    }
    times(&utms);
    printf("Processus :%d, Le temps de parcours Ligne-Colonne est %ld tops d'horloge\n",
          getpid(), utms.tms_utime - start_time);
  }
  else {
    times(&utms);
    start_time = utms.tms_utime;
    for (j = 0; j < BLOCKSIZE; j++) {
      for (i = 0; i < ASSEZGRAND; i++) {
        k = i * BLOCKSIZE + j;
        *(array + k) = 'a';
      }
    }
    times(&utms);
    printf("Processus :%d, Le temps de parcours Colonne-Ligne est %ld tops d'horloge\n",
          getpid(), utms.tms_utime - start_time);
  }

  getrusage(RUSAGE_SELF, &usage);
  printf("Le nombre de demande de pages est %ld\n", usage.ru_minflt);
  printf("Le nombre de fautes de pages est %ld\n", usage.ru_majflt);

  free(r);
  free(array);

  return EXIT_SUCCESS;
}
