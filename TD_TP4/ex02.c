/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex02.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/26 22:43:49 by NoobZik           #+#    #+#             */
/*   Updated: 2019/02/27 11:20:19 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/** Algorithm LRU **/

#include  <stdio.h>
#include  <stdlib.h>
#include  <sys/types.h>
#include  <sys/stat.h>
#include  <fcntl.h>
#include  <unistd.h>

#define  MAX_PAGES 31
/**
 * Cette structure définit simplement une page.
 * - Le premier champs "ref" est l'adresse du cadre où se trouve en mémoire
 * centrale la page correspondante du programme.
 * - Le deuxième champ "id" laisse supposer (d'après le cours) que
 */
typedef struct page {
  int ref;
  int id;
}PAGE;

int suis_je_plus_anc (int plus_anc ,PAGE p);
int default_de_page (PAGE p[], int nb_pages, int page);
int chercher_nouvelle_anc (PAGE p[], int nb_pages);


/**
 * [suis_je_plus_anc  description]
 * @param  plus_anc [description]
 * @param  p        Structure de page
 * @return          Renvoie 0 si ce n'est pas le plus ancien
 *                  Renvoie 1 si c'est le plus ancien
 */
int suis_je_plus_anc (int plus_anc, PAGE p) {
  return (plus_anc < p.id) ? 1 : 0;
}

/**
 * Je pense que cette fonction doit retourner le nombre de defaut de pages
 * @param  p        [description]
 * @param  nb_pages [description]
 * @param  page     [description]
 * @return          [description]
 */
int  default_de_page (PAGE p[], int nb_pages, int page) {
  int i;
  for (i = 0; i < nb_pages; i++) {
    if (p[i])
  }
}

/**
 * [chercher_nouvelle_anc  description]
 * @param  p        [description]
 * @param  nb_pages [description]
 * @return          [description]
 */
int  chercher_nouvelle_anc (PAGE p[], int  nb_pages) {
}

int  main(int argc , char *argv []) {
  char* buffer;
  PAGE  pages[MAX_PAGES];
  int   nb_pages ;
  int   plus_ancienne;

  /* la plus  ancienne  non  referciee  */
  int fd ;
  int i, idx_page;
  int temps=0, b;

  struct  stat  statf;
  off_t         size ;

  /* Pour  les  statistiques  */
  int  nb_defaults=0;
  int  nb_demandes=0 ;
  ...........

  return 0;
}
